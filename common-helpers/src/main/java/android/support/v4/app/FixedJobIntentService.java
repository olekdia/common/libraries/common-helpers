package androidx.core.app;

public abstract class FixedJobIntentService extends JobIntentService {

    @Override
    JobIntentService.GenericWorkItem dequeueWork() {
        try {
            return super.dequeueWork();
        } catch (SecurityException ignored) {
        }
        return null;
    }
}