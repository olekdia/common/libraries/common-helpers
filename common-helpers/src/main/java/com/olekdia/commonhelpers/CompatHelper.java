package com.olekdia.commonhelpers;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import java.io.File;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.StyleRes;
import androidx.core.content.ContextCompat;

public class CompatHelper {
    private static final String HUAWEI_MANUFACTURER = "Huawei";


    // IMPORTANT TO USE THIS METHOD, INSTEAD OF Context.getExternalFilesDir(..)!!!!
    // There is a bug on Huawei-Devices, which select the wrong external path.
    // More info here: https://stackoverflow.com/questions/39895579/fileprovider-error-onhuawei-devices
    public static File getExternalCacheDir(final Context context) {
        if (HUAWEI_MANUFACTURER.equalsIgnoreCase(android.os.Build.MANUFACTURER)) {
            final File[] dirs = ContextCompat.getExternalCacheDirs(context);
            if (dirs.length > 0) {
                return dirs[0];
            } else {
                return context.getExternalCacheDir();
            }
        } else {
            return context.getExternalCacheDir();
        }
    }

    // Same as above
    public static File getExternalFileDir(final Context context, final String type) {
        if (HUAWEI_MANUFACTURER.equalsIgnoreCase(android.os.Build.MANUFACTURER)) {
            final File[] dirs = ContextCompat.getExternalFilesDirs(context, type);
            if (dirs.length > 0) {
                return dirs[0];
            } else {
                return context.getExternalFilesDir(type);
            }
        } else {
            return context.getExternalFilesDir(type);
        }
    }

    @SuppressWarnings("NewApi")
    public static String getBestDateTimePattern(final Locale locale, final String skeleton) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2
                ? DateFormat.getBestDateTimePattern(locale, skeleton)
                : skeleton;
    }

    public static void setBackgroundCompat(final View v, final Drawable drawable) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            v.setBackground(drawable);
        } else {
            v.setBackgroundDrawable(drawable);
        }
    }

    @SuppressWarnings("deprecation")
    public static void setTextAppearanceCompat(@NonNull final TextView textView, @StyleRes final int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setTextAppearance(resId);
        } else {
            textView.setTextAppearance(textView.getContext(), resId);
        }
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtmlCompat(final String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }

    public static void removeOnGlobalLayoutListenerCompat(View v, ViewTreeObserver.OnGlobalLayoutListener listener) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            v.getViewTreeObserver().removeGlobalOnLayoutListener(listener);
        } else {
            v.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
        }
    }

    @SuppressWarnings("deprecation")
    public static void changeEdgeEffect(final Resources res, final int color) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            try {
                final int glowDrawableId = res.getIdentifier("overscroll_glow", "drawable", "android");
                final Drawable androidGlow = res.getDrawable(glowDrawableId);
                if (androidGlow != null) {
                    androidGlow.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
                }
            } catch (Resources.NotFoundException e) {
                // do nothing
            }
        }
    }
}