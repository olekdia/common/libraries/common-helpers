package com.olekdia.commonhelpers.arrays;

import com.olekdia.commonhelpers.CommonHelper;

/**
 * Frozen to release
 */
public class FastLongStack {

    /**
     * Default initial capacity.
     */
    private static final int DEFAULT_CAPACITY = 10;
    /**
     * Default initial capacity increment
     */
    private static final int DEFAULT_CAP_INCREMENT = 5;

    protected long[] mArray;

    /**
     * The number of elements or the size of the vector.
     */
    protected int mSize;
    /**
     * How many elements should be added to the vector when it is detected that
     * it needs to grow to accommodate extra entries. If this value is zero or
     * negative the size will be doubled if an increase is needed.
     */
    protected int mCapacityInc;

    public FastLongStack(final int initialCapacity, final int initialCapIncrement) {
        if (initialCapacity > 0) {
            mArray = new long[initialCapacity];
        } else if (initialCapacity == 0) {
            mArray = ArrayHelper.EMPTY_LONGS;
        }
        mCapacityInc = Math.min(initialCapIncrement, 0);
    }

    public FastLongStack(int initialCapacity) {
        this(initialCapacity, DEFAULT_CAP_INCREMENT);
    }

    public FastLongStack() {
        this(DEFAULT_CAPACITY, DEFAULT_CAP_INCREMENT);
    }

    public final boolean isEmpty() {
        return mSize == 0;
    }

    public final int size() {
        return mSize;
    }

    public final long push(final long value) {
        if (mSize == mArray.length) {
            grow();
        }
        mArray[mSize++] = value;
        return value;
    }

    public final void pushDistinct(final long value) {
        if (!contains(value)) push(value);
    }

    public final long pop() {
        if (mSize == 0) {
            return CommonHelper.INVALID;
        } else {
            final int index = --mSize;
            final long value = mArray[index];
            mArray[index] = CommonHelper.INVALID;
            return value;
        }
    }

    public final long get(final int index) {
        return mArray[index];
    }

    public final int indexOf(final long value) {
        final long[] arr = mArray;
        final int size = mSize;
        for (int i = 0; i < size; i++) {
            if (value == arr[i]) return i;
        }
        return -1;
    }

    public final boolean contains(final long value) {
        return indexOf(value) >= 0;
    }


    public final void clear() {
        if (mSize != 0) mSize = 0;
    }

    public final boolean remove(final long value) {
        final int index = indexOf(value);
        if (index == CommonHelper.INVALID) {
            return false;
        } else {
            final int numMoved = mSize - index - 1;
            if (numMoved > 0) System.arraycopy(mArray, index + 1, mArray, index, numMoved);
            mSize--;
            return true;
        }
    }

    public final void setCapacityIncrement(final int value) {
        mCapacityInc = value;
    }

    private void grow() {
        grow(mCapacityInc <= 0 ? DEFAULT_CAP_INCREMENT : mCapacityInc);
    }

    private void grow(final int additionalSize) {
        final long[] newArray = new long[mArray.length + additionalSize];
        System.arraycopy(mArray, 0, newArray, 0, mSize);
        mArray = newArray;
    }

    public final long[] toArray(long[] contents) {
        final int s = mSize;
        if (contents.length < s) {
            contents = new long[s];
        }
        System.arraycopy(mArray, 0, contents, 0, s);
        return contents;
    }
}