package com.olekdia.commonhelpers.arrays;

import com.olekdia.commonhelpers.CommonHelper;

import java.lang.reflect.Array;

/**
 * Frozen to release
 */
public class FastStack<E> {

    /**
     * Default initial capacity.
     */
    private static final int DEFAULT_CAPACITY = 10;
    /**
     * Default initial capacity increment
     */
    private static final int DEFAULT_CAP_INCREMENT = 5;

    protected Object[] mArray;

    /**
     * The number of elements or the size of the vector.
     */
    protected int mSize;
    /**
     * How many elements should be added to the vector when it is detected that
     * it needs to grow to accommodate extra entries. If this value is zero or
     * negative the size will be doubled if an increase is needed.
     */
    protected int mCapacityInc;

    public FastStack(final int initialCapacity, final int initialCapIncrement) {
        if (initialCapacity > 0) {
            mArray = new Object[initialCapacity];
        } else if (initialCapacity == 0) {
            mArray = ArrayHelper.EMPTY_OBJECTS;
        }
        mCapacityInc = Math.min(initialCapIncrement, 0);
    }

    public FastStack(int initialCapacity) {
        this(initialCapacity, DEFAULT_CAP_INCREMENT);
    }

    public FastStack() {
        this(DEFAULT_CAPACITY, DEFAULT_CAP_INCREMENT);
    }

    public final boolean isEmpty() {
        return mSize == 0;
    }

    public final int size() {
        return mSize;
    }

    public final E push(final E object) {
        if (mSize == mArray.length) grow();

        mArray[mSize++] = object;
        return object;
    }

    public final boolean pushAll(final E[] a) {
        final int numNew = a.length;
        ensureCapacity(mSize + numNew);  // Increments modCount

        System.arraycopy(a, 0, mArray, mSize, numNew);
        mSize += numNew;
        return numNew != 0;
    }

    public final void pushDistinct(final E object) {
        if (!contains(object)) push(object);
    }

    @SuppressWarnings("unchecked")
    public final E pop() {
        if (mSize == 0) {
            return null;
        } else {
            final int index = --mSize;
            final E obj = (E) mArray[index];
            mArray[index] = null;
            return obj;
        }
    }

    public final E get(final int index) {
        return (E) mArray[index];
    }

    public final int indexOf(final Object o) {
        final Object[] arr = mArray;
        final int size = mSize;
        if (o == null) {
            for (int i = 0; i < size; i++)
                if (arr[i] == null) return i;
        } else {
            for (int i = 0; i < size; i++)
                if (o.equals(arr[i])) return i;
        }
        return -1;
    }

    public final boolean contains(final Object o) {
        return indexOf(o) >= 0;
    }

    public final void clear() {
        for (int i = 0; i < mSize; i++) mArray[i] = null;

        mSize = 0;
    }

    public final boolean remove(final Object o) {
        final int index = indexOf(o);
        if (index == CommonHelper.INVALID) {
            return false;
        } else {
            final int numMoved = mSize - index - 1;
            if (numMoved > 0) System.arraycopy(mArray, index + 1, mArray, index, numMoved);
            mSize--;
            return true;
        }
    }

    public final void setCapacityIncrement(final int value) {
        mCapacityInc = value;
    }

    public final void ensureCapacity(final int capacity) {
        if (mArray.length < capacity) {
            grow(capacity - mArray.length);
        }
    }

    @SuppressWarnings("unchecked")
    private void grow() {
        grow(mCapacityInc <= 0 ? DEFAULT_CAP_INCREMENT : mCapacityInc);
    }

    @SuppressWarnings("unchecked")
    private void grow(final int additionalSize) {
        final E[] newData = (E[]) new Object[mArray.length + additionalSize];
        System.arraycopy(mArray, 0, newData, 0, mSize);
        mArray = newData;
    }

    public final <T> T[] toArray(T[] contents) {
        final int s = mSize;
        if (contents.length < s) {
            @SuppressWarnings("unchecked") T[] newArray
                    = (T[]) Array.newInstance(contents.getClass().getComponentType(), s);
            contents = newArray;
        }
        System.arraycopy(mArray, 0, contents, 0, s);
        if (contents.length > s) {
            contents[s] = null;
        }
        return contents;
    }
}