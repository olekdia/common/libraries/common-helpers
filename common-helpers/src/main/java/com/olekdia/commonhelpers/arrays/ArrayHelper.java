package com.olekdia.commonhelpers.arrays;

import androidx.annotation.Nullable;

import com.olekdia.commonhelpers.CommonHelper;
import com.olekdia.commonhelpers.MathHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class ArrayHelper {

    public static final int[] EMPTY_INTS = {};
    public static final long[] EMPTY_LONGS = {};
    public static final boolean[] EMPTY_BOOLS = {};
    public static final Object[] EMPTY_OBJECTS = {};

    public static int binarySearch(final long[] array, final int size, final long value) {
        int lo = 0;
        int hi = size - 1;

        int mid;
        long midVal;
        while (lo <= hi) {
            mid = (lo + hi) >>> 1;
            midVal = array[mid];

            if (midVal < value) {
                lo = mid + 1;
            } else if (midVal > value) {
                hi = mid - 1;
            } else {
                return mid;  // value found
            }
        }
        return ~lo;  // value not present
    }

    public static int binarySearch(final int[] array, final int size, final int value) {
        int lo = 0;
        int hi = size - 1;

        int mid, midVal;
        while (lo <= hi) {
            mid = (lo + hi) >>> 1;
            midVal = array[mid];

            if (midVal < value) {
                lo = mid + 1;
            } else if (midVal > value) {
                hi = mid - 1;
            } else {
                return mid;  // value found
            }
        }
        return ~lo;  // value not present
    }

    public static int idealIntArraySize(final int need) {
        return idealByteArraySize(need * 4) / 4;
    }

    public static int idealByteArraySize(final int need) {
        for (int i = 4; i < 32; i++)
            if (need <= (1 << i) - 12)
                return (1 << i) - 12;

        return need;
    }

    public static ArrayList<Integer> asArrayList(final int[] array) {
        final int size = array.length;
        final ArrayList<Integer> arrayList = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            arrayList.add(array[i]);
        }
        return arrayList;
    }

    public static ArrayList<Long> asArrayList(final long[] array) {
        final int size = array.length;
        final ArrayList<Long> arrayList = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            arrayList.add(array[i]);
        }
        return arrayList;
    }

    public static ArrayList<Boolean> asArrayList(final boolean[] array) {
        final int size = array.length;
        final ArrayList<Boolean> arrayList = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            arrayList.add(array[i]);
        }
        return arrayList;
    }

    public static long[] asLongArray(final List<Long> list) {
        final int size = list.size();
        final long[] array = new long[size];
        for (int i = 0; i < size; i++) {
            array[i] = list.get(i);
        }
        return array;
    }

    public static long[] asLongArray(final int[] intArray) {
        final int size = intArray.length;
        final long[] longArray = new long[size];
        for (int i = 0; i < size; i++) {
            longArray[i] = intArray[i];
        }
        return longArray;
    }

    public static int[] asIntArray(final List<Integer> list) {
        final int size = list.size();
        final int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = list.get(i);
        }
        return array;
    }

    public static boolean[] asBoolArray(final List<Boolean> list) {
        final int size = list.size();
        final boolean[] array = new boolean[size];
        for (int i = 0; i < size; i++) {
            array[i] = list.get(i);
        }
        return array;
    }

    public static int indexOfArray(final int[] array, final int key) {
        for (int i = array.length - 1; i >= 0; i--) {
            if (key == array[i]) return i;
        }
        return CommonHelper.INVALID;
    }

    public static int indexOfArray(final long[] array, final long key) {
        for (int i = array.length - 1; i >= 0; i--) {
            if (key == array[i]) return i;
        }
        return CommonHelper.INVALID;
    }

    public static int indexOfArray(final String[] array, final String key) {
        for (int i = array.length - 1; i >= 0; i--) {
            if (key.equals(array[i])) return i;
        }
        return CommonHelper.INVALID;
    }

    public static int indexOfArray(final Object[] array, final Object key) {
        for (int i = array.length - 1; i >= 0; i--) {
            if (key == array[i]) return i;
        }
        return CommonHelper.INVALID;
    }

    public static int[] addToArray(final int[] a, final int element) {
        final int[] result = Arrays.copyOf(a, a.length + 1);
        result[result.length - 1] = element;
        return result;
    }

    public static long[] addToArray(final long[] a, final long element) {
        final long[] result = Arrays.copyOf(a, a.length + 1);
        result[result.length - 1] = element;
        return result;
    }

    public static long[] removeFromArray(final long[] a, final long element) {
        final int index = indexOfArray(a, element);
        final long[] result = new long[a.length - 1];
        System.arraycopy(a, 0, result, 0, index);
        if (index < a.length - 1) {
            System.arraycopy(a, index + 1, result, index, a.length - index - 1);
        }
        return result;
    }

    public static long[] removeFromArray(final long[] a, final int index) {
        final long[] result = new long[a.length - 1];
        System.arraycopy(a, 0, result, 0, index);
        if (index < a.length - 1) {
            System.arraycopy(a, index + 1, result, index, a.length - index - 1);
        }
        return result;
    }

    @Nullable
    public static Set<String> copySet(final Set<String> inputSet) {
        return inputSet == null ? null : new HashSet<>(inputSet);
    }

    @Nullable
    public static void fillArray(final float[] in, final float[] out) {
        System.arraycopy(in, 0, out, 0, in.length);
    }

    public static void shuffleList(final List list) {
        final int count = list.size() - 1;
        for (int j, i = 0; i <= count; i++) {
            j = MathHelper.getRandom(0, count);
            Collections.swap(list, i, j);
        }
    }

    public static void shuffleList(final List list, final Random rand) {
        final int count = list.size() - 1;
        final int size = list.size();
        for (int j, i = 0; i <= count; i++) {
            j = rand.nextInt(size);
            Collections.swap(list, i, j);
        }
    }
}