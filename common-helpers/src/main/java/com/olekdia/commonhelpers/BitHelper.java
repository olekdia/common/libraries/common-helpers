package com.olekdia.commonhelpers;

public class BitHelper {

    public static int bitToIndex(final int bitSet) {
        return Integer.numberOfTrailingZeros(bitSet);
    }

    public static boolean isBitEnabled(final int bitSet, final int bit) {
        return (bitSet & bit) > 0;
    }

    public static boolean isBitIndexEnabled(final int bitSet, final int bitIndex) {
        return ((bitSet & (1 << bitIndex)) > 0);
    }

    public static int addBit(final int bitSet, final int bit) {
        return bitSet | bit;
    }

    public static long addBit(final long bitSet, final long bit) {
        return bitSet | bit;
    }

    public static int subtractBit(final int bitSet, final int bit) {
        return bitSet & ~bit;
    }

    public static long subtractBit(final long bitSet, final long bit) {
        return bitSet & ~bit;
    }

    public static int addBitIndex(int bitSet, final int bitIndex) {
        bitSet |= (1 << bitIndex);
        return bitSet;
    }

    public static long addBitIndex(long bitSet, final int bitIndex) {
        bitSet |= (1 << bitIndex);
        return bitSet;
    }

    public static int subtractBitIndex(int bitSet, final int bitIndex) {
        bitSet &= ~(1 << bitIndex);
        return bitSet;
    }

    public static long subtractBitIndex(long bitSet, final int bitIndex) {
        bitSet &= ~(1 << bitIndex);
        return bitSet;
    }

    @SuppressWarnings("ForLoopReplaceableByForEach")
    public static int addBitIndexes(int bitSet, int ... args) {
        for (int i = 0, count = args.length; i < count; i++) {
            bitSet = addBitIndex(bitSet, args[i]);
        }
        return bitSet;
    }

    @SuppressWarnings("ForLoopReplaceableByForEach")
    public static int subtractBitIndexes(int bitSet, int ... args) {
        for (int i = 0, count = args.length; i < count; i++) {
            bitSet = subtractBitIndex(bitSet, args[i]);
        }
        return bitSet;
    }
}