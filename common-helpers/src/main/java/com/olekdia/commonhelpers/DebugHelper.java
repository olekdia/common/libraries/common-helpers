package com.olekdia.commonhelpers;

import android.util.Log;

import androidx.collection.SimpleArrayMap;

public final class DebugHelper {

    public static final String TAG = "DebugHelper";

    public static SimpleArrayMap<String, Long> sMeasureMap = new SimpleArrayMap<>();

    public static void startMeasureNano() {
        startMeasureNano(TAG);
    }
    public static void startMeasureNano(final String tag) {
        sMeasureMap.put(tag, System.nanoTime());
    }
    public static void stopMeasureNano() {
        stopMeasureNano(TAG);
    }
    public static long stopMeasureNano(final String tag) {
        if (sMeasureMap.containsKey(tag)) {
            final long result = System.nanoTime() - sMeasureMap.get(tag);
            Log.d(tag, result + " ns");
            return result;
        } else {
            return 0L;
        }
    }

    public static void startMeasure() {
        startMeasure(TAG);
    }
    public static void startMeasure(final String tag) {
        sMeasureMap.put(tag, System.currentTimeMillis());
    }
    public static void stopMeasure() {
        stopMeasure(TAG);
    }
    public static long stopMeasure(final String tag) {
        if (sMeasureMap.containsKey(tag)) {
            final long result = System.currentTimeMillis() - sMeasureMap.get(tag);
            Log.d(tag, result + " ms");
            return result;
        } else {
            return 0L;
        }
    }

    public static void log(final String tag, int ... args) {
        String result = "";
        for (int arg : args) {
            result += arg + ", ";
        }
        Log.d(tag, result);
    }

    public static void log(final String tag, float ... args) {
        String result = "";
        for (float arg : args) {
            result += arg + ", ";
        }
        Log.d(tag, result);
    }

    public static void log(final String tag, double ... args) {
        String result = "";
        for (double arg : args) {
            result += arg + ", ";
        }
        Log.d(tag, result);
    }

    public static void log(final String tag, long ... args) {
        String result = "";
        for (long arg : args) {
            result += arg + ", ";
        }
        Log.d(tag, result);
    }

    public static void log(final String tag, boolean... args) {
        String result = "";
        for (boolean arg : args) {
            result += arg + ", ";
        }
        Log.d(tag, result);
    }

    public static void log(final String tag, String... args) {
        String result = "";
        for (String arg : args) {
            result += arg + ", ";
        }
        Log.d(tag, result);
    }

    public static void log(final String value) {
        Log.d(TAG, value);
    }
}