package com.olekdia.commonhelpers;

import android.content.Context;
import android.graphics.Typeface;
import androidx.collection.SimpleArrayMap;

public class FontHelper {

    // Typefaces
    public static final String ROBOTO_C_BOLD_FONT = "RobotoCondensed-Bold";
    public static final String ROBOTO_C_LIGHT_FONT = "RobotoCondensed-Light";
    public static final String ROBOTO_MEDIUM_FONT = "Roboto-Medium";

    private static final SimpleArrayMap<String, Typeface> sCache = new SimpleArrayMap<>();

    /*
    Each call to Typeface.createFromAsset will load a new instance of the typeface into memory,
    and this memory is not consistently get garbage collected
    http://code.google.com/p/android/issues/detail?id=9904
    (It states released but even on Lollipop you can see the typefaces accumulate even after
    multiple GC passes)
    You can detect this by running:
    adb shell dumpsys meminfo com.your.packagenage
    You will see output like:
     Asset Allocations
        zip:/data/app/com.your.packagenage-1.apk:/assets/Roboto-Medium.ttf: 125K
        zip:/data/app/com.your.packagenage-1.apk:/assets/Roboto-Medium.ttf: 125K
        zip:/data/app/com.your.packagenage-1.apk:/assets/Roboto-Medium.ttf: 125K
        zip:/data/app/com.your.packagenage-1.apk:/assets/Roboto-Regular.ttf: 123K
        zip:/data/app/com.your.packagenage-1.apk:/assets/Roboto-Medium.ttf: 125K
    */
    public static Typeface getTypeface(final Context ctx, final String name) {
        synchronized (sCache) {
            if (!sCache.containsKey(name)) {
                final Typeface t = Typeface.createFromAsset(ctx.getAssets(), "fonts/" + name + ".ttf");
                sCache.put(name, t);
                return t;
            }
            return sCache.get(name);
        }
    }
}