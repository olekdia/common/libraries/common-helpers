package com.olekdia.commonhelpers;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;
import android.view.View;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Locale;

import androidx.annotation.IntDef;

public class LocaleHelper {

    public static final char ZERO_DIGIT = '0';
    public static final char NINE_DIGIT = '9';

    /**
     * Unicode "Left-To-Right Embedding"
     */
    public static final char LRE = '\u202A';
    /**
     * Unicode "Right-To-Left Embedding"
     */
    public static final char RLE = '\u202B';
    /**
     * Unicode "Pop Directional Formatting"
     */
    public static final char PDF = '\u202C';
    /**
     * Unicode "Pop Directional Isolate"
     */
    public static final char PDI = '\u2069';
    /**
     * Left-To-Right Mark
     */
    public static final char LRM = '\u200E';
    /**
     * Right-To-Left Mark
     */
    public static final char RLM = '\u200F';
    /**
     * Left-To-Right Isolated
     */
    public static final char LRI = '\u2066';
    /**
     * Right-To-Left Isolated
     */
    public static final char RLI = '\u2067';
    /**
     * First-Strong Isolated
     */
    public static final char FSI = '\u2068';

    public static final char LRO = '\u202D';

    public static final char RLO = '\u202E';

    public static final String AR_LANG = "ar";
    public static final String BG_LANG = "bg";
    public static final String CS_LANG = "cs";
    public static final String FA_LANG = "fa";
    public static final String GU_LANG = "gu";
    public static final String MR_LANG = "mr";
    public static final String TH_LANG = "th";
    public static final String IW_LANG = "iw";
    public static final String PL_LANG = "pl";
    public static final String RU_LANG = "ru";
    public static final String UK_LANG = "uk";
    public static final String ZH_LANG = "zh";

    public static final String[] NUMBERS_0_31_WESTERN_ARABIC = {
            "0",
            "1",  "2",  "3",  "4",  "5",
            "6",  "7",  "8",  "9",  "10",
            "11", "12", "13", "14", "15",
            "16", "17", "18", "19", "20",
            "21", "22", "23", "24", "25",
            "26", "27", "28", "29", "30",
            "31"
    };
    public static final String[] NUMBERS_0_31_EASTERN_ARABIC = {
            "٠",
            "١",  "٢",  "٣",  "٤",  "٥",
            "٦",  "٧",  "٨",  "٩",  "١٠",
            "١١", "١٢", "١٣", "١٤", "١٥",
            "١٦", "١٧", "١٨", "١٩", "٢٠",
            "٢١", "٢٢", "٢٣", "٢٤", "٢٥",
            "٢٦", "٢٧", "٢٨", "٢٩", "٣٠",
            "٣١"
    };
    public static final String[] NUMBERS_0_31_PERSO_ARABIC = {
            "۰",
            "۱",  "۲",  "۳",  "۴",  "۵",
            "۶",  "۷",  "۸",  "۹",  "۱۰",
            "۱۱", "۱۲", "۱۳", "۱۴", "۱۵",
            "۱۶", "۱۷", "۱۸", "۱۹", "۲۰",
            "۲۱", "۲۲", "۲۳", "۲۴", "۲۵",
            "۲۶", "۲۷", "۲۸", "۲۹", "۳۰",
            "۳۱"
    };
    public static final String[] NUMBERS_0_31_GUJARATI = {
            "૦",
            "૧",  "૨",  "૩",  "૪",  "૫",
            "૬",  "૭",  "૮",  "૯",  "૧૦",
            "૧૧", "૧૨", "૧૩", "૧૪", "૧૫",
            "૧૬", "૧૭", "૧૮", "૧૯", "૨૦",
            "૨૧", "૨૨", "૨૩", "૨૪", "૨૫",
            "૨૬", "૨૭", "૨૮", "૨૯", "૩૦",
            "૩૧"
    };
    public static final String[] NUMBERS_0_31_DEVANAGARI = {
            "०",
            "१",  "२",  "३",  "४",  "५",
            "६",  "७",  "८",  "९",  "१०",
            "११", "१२", "१३", "१४", "१५",
            "१६", "१७", "१८", "१९", "२०",
            "२१", "२२", "२३", "२४", "२५",
            "२६", "२७", "२८", "२९", "३०",
            "३१"
    };
    public static final String[] NUMBERS_0_31_THAI = {
            "๐",
            "๑",  "๒",  "๓",  "๔",  "๕",
            "๖",  "๗",  "๘",  "๙",  "๑๐",
            "๑๑", "๑๒", "๑๓", "๑๔", "๑๕",
            "๑๖", "๑๗", "๑๘", "๑๙", "๒๐",
            "๒๑", "๒๒", "๒๓", "๒๔", "๒๕",
            "๒๖", "๒๗", "๒๘", "๒๙", "๓๐",
            "๓๑"
    };

    public static final char[] DIGITS_WESTERN_ARABIC = {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    };
    public static final char[] DIGITS_EASTERN_ARABIC = {
            '٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'
    };
    public static final char[] DIGITS_PERSO_ARABIC = {
            '۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'
    };
    public static final char[] DIGITS_GUJARATI = {
            '૦', '૧', '૨', '૩', '૪', '૫', '૬', '૭', '૮', '૯'
    };
    public static final char[] DIGITS_DEVANAGARI = {
            '०', '१', '२', '३', '४', '५', '६', '७', '८', '९'
    };
    public static final char[] DIGITS_THAI = {
            '๐', '๑', '๒', '๓', '๔', '๕', '๖', '๗', '๘', '๙'
    };

    public static final int WESTERN_ARABIC = 0;
    public static final int EASTERN_ARABIC = 1;
    public static final int PERSO_ARABIC = 2;
    public static final int GUJARATI = 3;
    public static final int DEVANAGARI = 4;
    public static final int THAI = 5;
    @IntDef({WESTERN_ARABIC, EASTERN_ARABIC, PERSO_ARABIC, GUJARATI, DEVANAGARI, THAI})
    @Retention(RetentionPolicy.SOURCE)
    public @interface NumeralSystem {}

    public static final int AM = 0;
    public static final int PM = 1;
    @IntDef({AM, PM})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TimeFormat {}

    private static final StringBuilder sFmtBuilder = new StringBuilder(100);

    public static boolean isLayoutRtl(final Resources res) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return res.getConfiguration().getLayoutDirection() == View.LAYOUT_DIRECTION_RTL;
        } else {
            switch (Locale.getDefault().getLanguage()) {
                case AR_LANG:
                case FA_LANG:
                case IW_LANG:
                    return true;

                default:
                    return false;
            }
        }
    }

    public static boolean isAppLangDiff(final Context context, final String prefLang) {
        final Configuration appConfig = context.getResources().getConfiguration();
        final Configuration sysConfig = Resources.getSystem().getConfiguration();

        final String appLang = getLocale(appConfig).getLanguage();
        final String sysLang = getLocale(sysConfig).getLanguage();

        if ("sys".equals(prefLang)) {
            return !appLang.equals(sysLang);
        } else {
            return !appLang.equals(prefLang)
                    || ZH_LANG.equals(prefLang);
        }
    }

    public static Context changeLanguageIfDiff(final Context context, final String prefLang) {
        return isAppLangDiff(context, prefLang)
                ? changeLanguage(context, prefLang)
                : context;
    }

    public static Context changeLanguage(final Context context, final String toLang) {
        final Resources res = context.getResources();
        final Configuration config = res.getConfiguration();

        final Locale toLocale = toLang.contains(ZH_LANG)
                ? (toLang.contains("rCN")
                ? Locale.SIMPLIFIED_CHINESE : Build.VERSION.SDK_INT >= Build.VERSION_CODES.N
                ? new Locale(ZH_LANG, "Hant") : Locale.TRADITIONAL_CHINESE)
                : new Locale(toLang);

        Locale.setDefault(toLocale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(toLocale);

            final LocaleList localeList = new LocaleList(toLocale);
            LocaleList.setDefault(localeList);
            config.setLocales(localeList);
        } else {
            config.locale = toLocale;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLayoutDirection(toLocale);
            return context.createConfigurationContext(config);
        } else {
            res.updateConfiguration(config, res.getDisplayMetrics());
            return context;
        }
    }

    public static Locale getLocale(Configuration config) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.N
                ? config.getLocales().get(0)
                : config.locale;
    }

    private static char[] getDigits(final int numSystem) {
        switch (numSystem) {
            default:
            case WESTERN_ARABIC:    return DIGITS_WESTERN_ARABIC;
            case EASTERN_ARABIC:    return DIGITS_EASTERN_ARABIC;
            case PERSO_ARABIC:      return DIGITS_PERSO_ARABIC;
            case GUJARATI:          return DIGITS_GUJARATI;
            case DEVANAGARI:        return DIGITS_DEVANAGARI;
            case THAI:              return DIGITS_THAI;
        }
    }

    private static String[] getNumbers31(final int numSystem) {
        switch (numSystem) {
            default:
            case WESTERN_ARABIC:    return NUMBERS_0_31_WESTERN_ARABIC;
            case EASTERN_ARABIC:    return NUMBERS_0_31_EASTERN_ARABIC;
            case PERSO_ARABIC:      return NUMBERS_0_31_PERSO_ARABIC;
            case GUJARATI:          return NUMBERS_0_31_GUJARATI;
            case DEVANAGARI:        return NUMBERS_0_31_DEVANAGARI;
            case THAI:              return NUMBERS_0_31_THAI;
        }
    }

    public static String formatNumber31(final int number, final int numSystem) {
        return getNumbers31(numSystem)[number];
    }

    /**
     * Converts only from western arabic
     */
    public static void convertNumbers(final StringBuilder b, final int numSystem) {
        final int count = b.length();
        if (count == 0 || numSystem == WESTERN_ARABIC) return;
        final char[] digits = getDigits(numSystem);

        char currChar;
        for (int i = 0; i < count; i++) {
            currChar = b.charAt(i);
            if (currChar >= ZERO_DIGIT && currChar <= NINE_DIGIT) {
                b.setCharAt(i, digits[currChar - ZERO_DIGIT]);
            }
        }
    }

    public static CharSequence convertNumbers(final CharSequence text, final int numSystem) {
        if (numSystem == WESTERN_ARABIC) return text;

        sFmtBuilder.setLength(0);
        sFmtBuilder.append(text);
        convertNumbers(sFmtBuilder, numSystem);
        return sFmtBuilder.toString();
    }

    public static CharSequence[] convertNumbers(final CharSequence[] arr, final int numSystem) {
        if (numSystem == WESTERN_ARABIC) return arr;

        for (int i = arr.length - 1; i >= 0; i--) {
            arr[i] = convertNumbers(arr[i], numSystem);
        }
        return arr;
    }

    /**
     * Converts only to western arabic
     */
    public static void convertToWesternArabicNumbers(final StringBuilder b, final int numSystem) {
        final int count = b.length();
        if (count == 0 || numSystem == WESTERN_ARABIC) return;
        final char[] digits = getDigits(numSystem);

        char currChar;
        for (int i = 0; i < count; i++) {
            currChar = b.charAt(i);
            if (currChar >= digits[0] && currChar <= digits[9]) {
                b.setCharAt(i, Character.forDigit(currChar - digits[0], 10));
            }
        }
    }

    public static CharSequence convertToWesternArabicNumbers(final CharSequence text, final int numSystem) {
        if (numSystem == WESTERN_ARABIC) return text;

        sFmtBuilder.setLength(0);
        sFmtBuilder.append(text);
        convertToWesternArabicNumbers(sFmtBuilder, numSystem);
        return sFmtBuilder.toString();
    }

    public static String formatInt(final int value, final int numSystem) {
        sFmtBuilder.setLength(0);
        sFmtBuilder.append(value);

        convertNumbers(sFmtBuilder, numSystem);
        return sFmtBuilder.toString();
    }

    public static String formatLong(final long value, final int numSystem) {
        sFmtBuilder.setLength(0);
        sFmtBuilder.append(value);

        convertNumbers(sFmtBuilder, numSystem);
        return sFmtBuilder.toString();
    }

    public static String formatIntTwoDig(final int value) {
        return formatIntTwoDig(value, WESTERN_ARABIC);
    }

    public static String formatIntTwoDig(final int value, final int numSystem) {
        sFmtBuilder.setLength(0);

        if (value <= 0) {
            sFmtBuilder.append(ZERO_DIGIT)
                       .append(ZERO_DIGIT);
        } else if (value < 10) {
            sFmtBuilder.append(ZERO_DIGIT)
                       .append(value);
        } else if (value >= 10) {
            sFmtBuilder.append(value);
        }

        convertNumbers(sFmtBuilder, numSystem);
        return sFmtBuilder.toString();
    }

    public static String formatIntThreeDig(final int value, final int numSystem) {
        sFmtBuilder.setLength(0);

        if (value <= 0) {
            sFmtBuilder.append(ZERO_DIGIT)
                       .append(ZERO_DIGIT)
                       .append(ZERO_DIGIT);
        } else if (value < 10) {
            sFmtBuilder.append(ZERO_DIGIT)
                       .append(ZERO_DIGIT)
                       .append(value);
        } else if (value >= 10 && value < 100) {
            sFmtBuilder.append(ZERO_DIGIT)
                       .append(value);
        } else if (value >= 100) {
            sFmtBuilder.append(value);
        }

        convertNumbers(sFmtBuilder, numSystem);
        return sFmtBuilder.toString();
    }
}