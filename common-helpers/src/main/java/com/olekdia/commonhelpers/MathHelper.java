package com.olekdia.commonhelpers;

import android.graphics.PointF;

import java.math.BigInteger;
import java.util.Random;

public class MathHelper {

    public static final int DEG_2PI = 360;
    public static final float PI = 3.141592653F;
    public static final float GOLDEN_RATIO = 1.61803F;

    public static int fastModulo(final int dividend, final int divisor) {
        return dividend & (divisor - 1);
    }

    public static boolean isRounded(final double value) {
        return value == (int) value;
    }

    public static double round(double value, final int places) {
        if (places < 0) throw new IllegalArgumentException();

        final long factor = (long) Math.pow(10, places);
        value = value * factor;
        return (double) Math.round(value) / factor;
    }

    public static float round(float value, final int places) {
        if (places < 0) throw new IllegalArgumentException();

        final int factor = (int) Math.pow(10, places);
        value = value * factor;
        return (float) Math.round(value) / factor;
    }

    public static float roundTo(final float value, final float to) {
        return value - Math.abs(value % to);
    }

    public static int roundTo(final float value, final int to) {
        return Math.round(value - value % to);
    }

    public static long roundTo(final double value, final int to) {
        return Math.round(value - value % to);
    }

    public static int roundTo(final int value, final int to) {
        return Math.round(value - value % to);
    }

    public static int roundToCeil(final float value, final int to) {
        if (value % to == 0) {
            return (int) value;
        } else {
            return Math.round(value - value % to) + to;
        }
    }

    /* To get integer part of a float */
    public static int getIntegerPart(final double d) {
        return d >= 0 ? (int) Math.floor(d) : (int) Math.floor(d) + 1;
    }

    public static int getPercentOfNumber(int number, int percent) {
        return number * percent / 100;
    }

    public static int numDigits(final int number) {
        return number == 0 ? 1 : number < 0 ? (int) Math.log10(-number) + 2 : (int) Math.log10(number) + 1;
    }

    public static int divideWithRoundUp(final int what, final int to) {
        return what / to + (what % to == 0 ? 0 : 1);
    }

    public static float normalize(final float value, final float min, final float max) {
        return value * (max - min) + min;
    }
    public static int log(final int a, final int x) {
        return (int) (Math.log(x) / Math.log(a));
    }

    public static int min(int... args) {
        int minResult = Integer.MAX_VALUE;
        for (int arg : args) {
            if (arg < minResult) {
                minResult = arg;
            }
        }
        return minResult;
    }

    public static int signum(final int i) {
        return i > 0 ? 1 : (i < 0 ? -1 : 0);
    }

    public static int signum(final long i) {
        return i > 0 ? 1 : (i < 0 ? -1 : 0);
    }

    public static int getRandom(final int min, final int max) {
        return (int) Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public static long getRandom(final long min, final long max) {
        return (long) Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public static float getRandom(final float min, final float max) {
        return (float) Math.random() * (max - min) + min;
    }

    public static double getRandom(final double min, final double max) {
        return Math.random() * (max - min) + min;
    }

    public static boolean getRandom() {
        return Math.random() > 0.5;
    }

    public static float getRandom(final float min, final float max, final Random random) {
        return random.nextFloat() * (max - min) + min;
    }

    public static int getRandom(final int min, final int max, final Random random) {
        return random.nextInt(max - min + 1) + min;
    }

    public static double toRadians(final double value) {
        return value * Math.PI / 180.0;
    }

    public static float toRadians(final float value) {
        return value * PI / 180F;
    }

    public static PointF toScreenCoordinates(final PointF center, final PointF point) {
        final PointF resultPoint = new PointF();
        resultPoint.x = point.x + center.x;
        resultPoint.y = center.y - point.y;
        return resultPoint;
    }

    /**
     * Transform coords
     *
     * @param point PointF to set new coords, allocated by the caller
     * @param cx    center x
     * @param cy    center y
     * @param px    point x
     * @param py    point y
     */
    public static void toScreenCoordinates(final PointF point, final float cx, final float cy, final float px, final float py) {
        point.set(px + cx, cy - py);
    }

    /**
     * Transforms point coords relative to the left-top display corner,
     * to the coords relative to the center point
     */
    public static PointF toRealCoords(final float x, final float y, final float cx, final float cy) {
        final PointF rp = new PointF();
        rp.x = x - cx;
        rp.y = y - cy;
        return rp;
    }

    public static boolean isInCircle(final float x, final float y, final float cx, final float cy, final float r) {
        return (x - cx) * (x - cx) + (y - cy) * (y - cy) <= r * r;
    }

    /**
     * Detect if the point is outside fillable area of the circle
     */
    public static boolean isOutsideCircle(final PointF p, final float radius, final float strokeWidth) {
        return !isWithinRadius(p, radius - strokeWidth / 2);
    }

    /**
     * Detect if the point are inside stroked circle (without inner fill)
     */
    public static boolean isWithinRing(final PointF p, final float radius, final float strokeWidth) {
        return isWithinRadius(p, radius + strokeWidth / 2) && !isWithinRadius(p, radius - strokeWidth / 2);
    }

    public static boolean isWithinRadius(final PointF p, final float radius) {
        return p.x * p.x + p.y * p.y <= radius * radius;
    }

    public static boolean areClockwise(final PointF p, final float cx, final float cy, final float radius, final float angle) {
        final float radAngle = toRadians(angle);
        final double sx = (cx + radius) * Math.cos(radAngle);
        final double sy = (cy + radius) * Math.sin(radAngle);
        return -sx * p.y + sy * p.x > 0;
    }

    public static boolean isInsideSector(PointF p, PointF center, float radius, float angle1, float angle2) {
        PointF realPoint = new PointF();
        realPoint.x = p.x - center.x;
        realPoint.y = p.y - center.y;
        return !areClockwise(realPoint, center.x, center.y, radius, angle1) &&
                areClockwise(realPoint, center.x, center.y, radius, angle2) &&
                isWithinRadius(realPoint, radius);
    }

    /**
     * Detects if the point inside some sector
     */
    public static boolean isInsideAngle(final PointF p, final float cx, final float cy, final float radius,
                                        final float angle1, final float angle2) {
        if (angle2 - angle1 <= 180F) {
            return !areClockwise(p, cx, cy, radius, angle1) && areClockwise(p, cx, cy, radius, angle2);
        } else {
            return isInsideAngle(p, cx, cy, radius, angle1, angle1 + 180F)
                    || isInsideAngle(p, cx, cy, radius, angle1 + 180F, angle2);
        }
    }

    public static double segmentLength(final float x1, final float y1, final float x2, final float y2) {
        return Math.sqrt((double) (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    public static int gcd(int a, int b) {
        int tmp;
        while(a != 0 && b != 0) {
            a %= b;
            tmp = a;
            a = b;
            b = tmp;
        }
        return a + b;
    }

    public static int lcm(final int a, final int b) {
        return a / gcd(a, b) * b;
    }

    public static int lcm(final int a, final int b, final int c) {
        return lcm(a, lcm(b,c));
    }

    public static double geomProgressionItemN(final long item1, final double q, final int n) {
        return (item1 * Math.pow(q, n - 1));
    }

    public static long permutationBI(final int n, final int k) {
        return factorialBI(n).divide(factorialBI(n - k)).longValue();
    }

    public static long factorial(int num) {
        long fact = 1;

        for (; num > 0; fact *= num--) ;

        return fact;
    }

    public static BigInteger factorialBI(int num) {
        BigInteger fact = BigInteger.valueOf(1);
        for (; num > 0; num--)
            fact = fact.multiply(BigInteger.valueOf(num));

        return fact;
    }

    public static long combinationBI(final int n, final int k) {
        return factorialBI(n).divide(factorialBI(k).multiply(factorialBI(n - k))).longValue();
    }

    public static long combination(final int n, final int k) {
        return factorial(n) / (factorial(k) * factorial(n - k));
    }

    public static long arrangementBI(final int n, final int k) {
        return factorialBI(n).divide(factorialBI(n - k)).longValue();
    }

    /**
     *
     * @param k True answers
     * @param n All answers
     * @param p probability in single round
     * @return
     */
    public static double bernoulli(final int k, final int n, final double p) {
        return combinationBI(n, k) * Math.pow(p, k) * Math.pow(1 - p, n - k);
    }
}