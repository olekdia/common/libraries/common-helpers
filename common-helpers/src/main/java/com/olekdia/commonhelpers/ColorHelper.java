package com.olekdia.commonhelpers;

import android.graphics.Color;
import androidx.annotation.ColorInt;

public class ColorHelper {

    public static final int ALPHA_MASK = 0x00FFFFFF;

    public static final int MAX_CHANEL = 255;

    public static float getAlphaPercent(final int argb) {
        return Color.alpha(argb) / 255F;
    }

    public static int alphaValueAsInt(final float alpha) {
        return Math.round(alpha * MAX_CHANEL);
    }

    @ColorInt
    public static int adjustAlpha(final int color, final float alpha) {
        return Math.round((color >>> 24) * alpha) << 24 | (ALPHA_MASK & color);
    }

    @ColorInt
    public static int replaceAlpha(final int color, final int alpha) {
        return alpha << 24 | (ALPHA_MASK & color);
    }

    @ColorInt
    public static int replaceAlpha(final int color, final float alpha) {
        return alphaValueAsInt(alpha) << 24 | (ALPHA_MASK & color);
    }

    @ColorInt
    public static int opaqueColor(final int color) {
        return MAX_CHANEL << 24 | (ALPHA_MASK & color);
    }

    public static int colorAtLightness(final int color, final float lightness) {
        final float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] = lightness;
        return Color.HSVToColor(hsv);
    }

    public static float lightnessOfColor(final int color) {
        final float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        return hsv[2];
    }

    @ColorInt
    public static int saturate(final int color, final float factor) {
        final float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[1] *= factor;
        return Color.HSVToColor(hsv);
    }

    @ColorInt
    public static int darker(final int color, final float factor) {
        return Color.argb(color >>> 24,
                          Math.min(Math.max((int) (((color >> 16) & MAX_CHANEL) * factor), 0), MAX_CHANEL),
                          Math.min(Math.max((int) (((color >> 8) & MAX_CHANEL) * factor), 0), MAX_CHANEL),
                          Math.min(Math.max((int) ((color & MAX_CHANEL) * factor), 0), MAX_CHANEL));
    }

    @ColorInt
    public static int toPastel(final int color) {
        return (MAX_CHANEL << 24)
                | (((((color >> 16) & MAX_CHANEL) + MAX_CHANEL) / 2) << 16)
                | (((((color >> 8) & MAX_CHANEL) + MAX_CHANEL) / 2) << 8)
                | (((color & MAX_CHANEL) + MAX_CHANEL) / 2);
    }

    /*public static int toPastel(final int color) {
        return Color.rgb((Color.red(color) + MAX_CHANEL) / 2,
                         (Color.green(color) + MAX_CHANEL) / 2,
                         (Color.blue(color) + MAX_CHANEL) / 2);
    }*/

    @ColorInt
    public static int mixColor(final int color, final int mixedChanel) {
        return Color.rgb((Color.red(color) + mixedChanel) / 2,
                         (Color.green(color) + mixedChanel) / 2,
                         (Color.blue(color) + mixedChanel) / 2);
    }

    @ColorInt
    public static int randomColor() {
        return randomColor(Color.WHITE);
    }

    @ColorInt
    public static int randomColor(final int mix) {
        return Color.rgb((int) (Math.random() * MAX_CHANEL + Color.red(mix)) / 2,
                         (int) (Math.random() * MAX_CHANEL + Color.green(mix)) / 2,
                         (int) (Math.random() * MAX_CHANEL + Color.blue(mix)) / 2);
    }

    public static boolean isColorDark(final int color) {
        double darkness = 1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255;
        return darkness >= 0.5;
    }
}