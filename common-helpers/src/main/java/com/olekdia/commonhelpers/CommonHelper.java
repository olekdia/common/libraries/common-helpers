package com.olekdia.commonhelpers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import java.util.Formatter;

import androidx.annotation.AttrRes;

public class CommonHelper {

    public static final int RESOURCE_NOT_FOUND = 0;
    public static final int INVALID = -1;

    public static void clearFormatter(final Formatter f) {
        ((StringBuilder) f.out()).setLength(0);
    }

    public static void cleanUpMemory() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    public static void sleepOnThread(final long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void recreateTask(final Context context) {
        final PackageManager pm = context.getPackageManager();
        final Intent intent = pm.getLaunchIntentForPackage(context.getPackageName());
        final ComponentName componentName = intent.getComponent();
        final Intent mainIntent = Intent.makeRestartActivityTask(componentName);
        context.startActivity(mainIntent);
        Runtime.getRuntime().exit(0);
    }

//--------------------------------------------------------------------------------------------------
//  Screen methods
//--------------------------------------------------------------------------------------------------

    public static float dpToPx(final float dp, final Resources res) {
        return dp * ((float) res.getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static int dpToPx(final int dp, final Resources res) {
        return (int) dpToPx((float) dp, res);
    }

    public static int spToPx(final float sp, final Resources res) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, res.getDisplayMetrics());
    }

    public static float pxToDp(final float px, final Resources res) {
        return px / ((float) res.getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static int getScreenSize(final Resources res) {
        return res.getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
    }

    public static boolean isLargeScreen(final Resources res) {
        final int screenSize = getScreenSize(res);
        return screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE
                || screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    public static boolean isSmallScreen(final Resources res) {
        return getScreenSize(res) == Configuration.SCREENLAYOUT_SIZE_SMALL;
    }

    public static boolean isPortOrientation(final Resources res) {
        return res.getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    public static boolean isLandOrientation(final Resources res) {
        return res.getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    public static int getOrientation(final Resources res) {
        return res.getConfiguration().orientation;
    }

    public static float getScreenHeight(final Resources res) {
        final DisplayMetrics displayMetrics = res.getDisplayMetrics();
        return displayMetrics.heightPixels / displayMetrics.density;
    }

    public static float getScreenWidth(final Resources res) {
        final DisplayMetrics displayMetrics = res.getDisplayMetrics();
        return displayMetrics.widthPixels / displayMetrics.density;
    }

//--------------------------------------------------------------------------------------------------
//  Cast methods
//--------------------------------------------------------------------------------------------------

    public static int boolToInt(final boolean value) {
        return value ? 1 : 0;
    }

    public static boolean intToBool(final int value) {
        return value > 0;
    }

    public static int idToIndex(final int id) {
        return id - 1;
    }

    public static int indexToId(final int index) {
        return index + 1;
    }

    public static int indexToCount(final int index) {
        return index + 1;
    }

//--------------------------------------------------------------------------------------------------
//  Network methods
//--------------------------------------------------------------------------------------------------

    @SuppressLint("MissingPermission")
    /**
     * In order to use add permission to manifest:
     * <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
     */
    public static boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) return false;

        final NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isAvailable() && netInfo.isConnected();
    }

//--------------------------------------------------------------------------------------------------
//  Resolve methods
//--------------------------------------------------------------------------------------------------

    public static boolean resolveBoolean(final Context context, @AttrRes final int attr) {
        return resolveBoolean(context, attr, false);
    }
    /**
     * Gets the required boolean value from the current context, if possible/available
     *
     * @param context  The context to use as reference for the boolean
     * @param attr     Attribute id to resolve
     * @param fallback Default value to return if no value is specified in theme
     * @return the boolean value from current theme
     */
    public static boolean resolveBoolean(final Context context, @AttrRes final int attr, final boolean fallback) {
        final TypedArray a = context.getTheme().obtainStyledAttributes(new int[]{attr});
        try {
            return a.getBoolean(0, fallback);
        } finally {
            a.recycle();
        }
    }

    public static Drawable resolveDrawable(final Context context, @AttrRes final int attr) {
        return resolveDrawable(context, attr, null);
    }
    private static Drawable resolveDrawable(final Context context, @AttrRes final int attr,
                                            @SuppressWarnings("SameParameterValue") final Drawable fallback) {
        final TypedArray a = context.getTheme().obtainStyledAttributes(new int[]{attr});
        try {
            Drawable d = a.getDrawable(0);
            if (d == null && fallback != null)
                d = fallback;
            return d;
        } finally {
            a.recycle();
        }
    }

    public static int resolveDimension(final Context context, @AttrRes final int attr) {
        return resolveDimension(context, attr, -1);
    }
    private static int resolveDimension(final Context context, @AttrRes final int attr, final int fallback) {
        final TypedArray a = context.getTheme().obtainStyledAttributes(new int[]{attr});
        try {
            return a.getDimensionPixelSize(0, fallback);
        } finally {
            a.recycle();
        }
    }

    public static int resolveColor(final Context context, @AttrRes final int attr) {
        return resolveColor(context, attr, 0);
    }
    public static int resolveColor(final Context context, @AttrRes final int attr, final int fallback) {
        final TypedArray a = context.getTheme().obtainStyledAttributes(new int[]{attr});
        try {
            return a.getColor(0, fallback);
        } finally {
            a.recycle();
        }
    }

    public static String resolveString(final Context context, @AttrRes final int attr) {
        final TypedValue v = new TypedValue();
        context.getTheme().resolveAttribute(attr, v, true);
        return (String) v.string;
    }

    public static Activity getActivity(Context context) {
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }

}