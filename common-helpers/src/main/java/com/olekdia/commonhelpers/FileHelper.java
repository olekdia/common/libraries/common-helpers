package com.olekdia.commonhelpers;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;

import java.io.IOException;
import java.nio.channels.FileChannel;

import androidx.annotation.NonNull;

public class FileHelper {

    public static void transferFile(final FileChannel src, final FileChannel dst) throws IOException {
        dst.transferFrom(src, 0, src.size());
        src.close();
        dst.close();
    }

    public static boolean isFilePath(final String value) {
        return value != null && value.startsWith("/");
    }

    public static boolean isDocumentFileUri(final String value) {
        return value != null && value.startsWith("content://");
    }

    public static String getFilePathFromFullPath(final String fullPath) {
        final int lastIndex = fullPath.lastIndexOf('/');
        return lastIndex > 0 ? fullPath.substring(0, lastIndex + 1) : fullPath;
    }

    public static String getFileNameFromFullPath(final String fullPath) {
        final int lastIndex = fullPath.lastIndexOf('/');
        return lastIndex > 0 ? fullPath.substring(lastIndex + 1) : fullPath;
    }

    public static String getFileNameWithoutExtension(final String fileName) {
        final int lastIndex = fileName.lastIndexOf('.');
        return lastIndex > 0 ? fileName.substring(0, lastIndex + 1) : fileName;
    }

    public static String getExtensionFromFileName(final String fileName) {
        final int lastIndex = fileName.lastIndexOf('.');
        return lastIndex >= 0 ? fileName.substring(lastIndex) : "";
    }

    public static String getFileNameFromUri(@NonNull final Uri uri, final ContentResolver cr) {
        String result = "";

        if (ContentResolver.SCHEME_FILE.equals(uri.getScheme())) {
            result = getFilePathFromFullPath(uri.getPath());
        } else if (ContentResolver.SCHEME_CONTENT.equals(uri.getScheme())) {
            try (final Cursor cursor = cr.query(uri, new String[]{OpenableColumns.DISPLAY_NAME}, null, null, null)) {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public static boolean isMediaDoc(final Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadDoc(final Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isExternalStoreDoc(final Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isGoogleDisk(final Uri uri) {
        return "com.google.android.apps.docs.storage".equals(uri.getAuthority());
    }
}