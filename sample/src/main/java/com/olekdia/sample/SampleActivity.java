package com.olekdia.sample;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.olekdia.commonhelpers.MathHelper;

import androidx.appcompat.app.AppCompatActivity;

public class SampleActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);

        findViewById(R.id.lcm_btn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        final Toast toast = Toast.makeText(this, "lcm 40, 7 =" + MathHelper.lcm(40, 7), Toast.LENGTH_LONG);
        toast.show();
    }
}
